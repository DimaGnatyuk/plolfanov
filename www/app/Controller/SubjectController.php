<?php
	class SubjectController extends AppController 
	{
		public function index(){
			$subjects = array('subject' => $this->Subject->find("first"));
			$this->set(compact("subjects"));
		}

		public function delete ($id){
			$this->Subject->delete($id);
       		$this->redirect(array('action'=>'edit'));
		}

		public function add ($param = "")
		{
			if ($param == "save")
			{
				$name = $_POST["name"];
				$teacher_id = $_POST["teacher_id"];

				$data = array("name" => $name, "teacher_id"=>$teacher_id);
				$this->Subject->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
					$this->loadModel("Teacher");
					$teachers = $this->Teacher->find("all");
					$this->set(compact("teachers"));
		}
		public function edit ($id = "", $param = "view")
		{
			if ($param == "view"){
				if ($id == "")
				{
					$subjects = $this->Subject->find("all");
					$this->set(compact("subjects"));
					$this->render("viewList");
				}
				else{
					$subject = $this->Subject->find("first",array('conditions'=>array("Subject.id"=>$id)));
					$this->loadModel("Teacher");
					$teachers = $this->Teacher->find("all");
					$this->set(compact("subject"));
					$this->set(compact("teachers"));
					$this->render("viewForm");
				}

			}elseif ($param == "update")
			{
				$name = $_POST["name"];
				$teacher_id = $_POST["teacher_id"];

				$data = array('id' => $id,"name" => $name, "teacher_id"=>$teacher_id);
				$this->Subject->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
		}
	}