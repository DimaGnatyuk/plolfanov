<?

	class RatingController extends AppController
	{
		
		function index($idStudent)
		{
			$ratings = array('ratings' => $this->Rating->find("first",array('conditions'=>array("rating.student_id"=>$idStudent))));
			$this->set(compact("ratings"));
			$back = $idStudent;
			$this->set(compact("back"));
		}

		public function delete ($id){
			$this->Rating->delete($id);
       		$this->redirect(array('action'=>'edit'));
		}

		public function add ($param = "")
		{
			if ($param == "save")
			{
				$student_id = $_POST["student_id"];
				$subject_id = $_POST["subject_id"];
				$rating = $_POST["rating"];

				$data = array("student_id" => $student_id, "subject_id"=>$subject_id, "rating" =>$rating);
				$this->Rating->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
					$this->loadModel("Subject");
					$this->loadModel("Student");
					$subjects = $this->Subject->find("all");
					$students = $this->Student->find("all");
					$this->set(compact("subjects"));
					$this->set(compact("students"));
		}
		public function edit ($id = "", $param = "view")
		{
			if ($param == "view"){
				if ($id == "")
				{
					$ratings = $this->Rating->find("all");
					$this->set(compact("ratings"));
					$this->render("viewList");
				}
				else{
					$rating = $this->Rating->find("first",array('conditions'=>array("Rating.id"=>$id)));
					$this->loadModel("Subject");
					$this->loadModel("Student");
					$subjects = $this->Subject->find("all");
					$students = $this->Student->find("all");
					$this->set(compact("subjects"));
					$this->set(compact("students"));
					$this->set(compact("rating"));
					$this->render("viewForm");
				}

			}elseif ($param == "update")
			{
				$student_id = $_POST["student_id"];
				$subject_id = $_POST["subject_id"];
				$rating = $_POST["rating"];

				$data = array("id"=>$id,"student_id" => $student_id, "subject_id"=>$subject_id, "rating" =>$rating);
				$this->Rating->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
		}
	}