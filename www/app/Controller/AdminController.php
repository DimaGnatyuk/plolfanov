<?php
	class AdminController extends AppController
	{
		
		function index($params = "")
		{
			if ($this->Session->read('admin') != "true")
			{
				if ($params == "")
				{
					$this->loadModel("Teacher");
					$teachers = $this->Teacher->find("all");
					$this->set(compact("teachers"));
					$this->render("form");
				}elseif ($params == "open")
				{
					$this->loadModel("Teacher");
					$login = $_POST["login"];
					$password = $_POST["password"];
					if ((strcmp($login,"admin") == 0)&&(strcmp($password,"admin") == 0))
					{
						$this->Session->write('admin', 'true');
					}else{
						$this->Session->write('admin', 'false');
						$this->redirect(array("controller" => "Group", 'action'=>'index'));
					}

				}
			}

			if ($params == "exit"){
				$this->Session->delete('admin');
				$this->redirect(array("controller" => "Group", 'action'=>'index'));
			}
		}
	}