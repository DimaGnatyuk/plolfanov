<?php
	class GroupController extends AppController {
		public function index() {
			$groups = $this->Group->find("all");
			$this->set(compact("groups"));
		}

		public function add ($param = "")
		{
			if ($param == "save")
			{
				$name = $_POST["name"];
				$data = array("name" => $name);
				$this->Group->Save($data);
				$groups = $this->Group->find("all");
				$this->set(compact("groups"));
				$this->render("viewList");
			}

		}

		public function delete ($id){
			$this->Group->delete($id);
       		$this->redirect(array('action'=>'edit'));
		}

		public function edit ($id = "", $param = "view")
		{
			if ($param == "view"){


				$group;
				if ($id == "")
				{
					$groups = $this->Group->find("all");
					$this->set(compact("groups"));
					$this->render("viewList");
				}
				else{
					$group = $this->Group->find("first",array('conditions'=>array("Group.id"=>$id)));
					$this->set(compact("group"));
					$this->render("viewForm");
				}

			}elseif ($param == "update")
			{
				$name = $_POST["name"];
				$data = array('id' => $id, "name" => $name);
				$this->Group->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
		}
	}