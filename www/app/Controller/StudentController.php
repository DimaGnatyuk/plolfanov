<?php 
	class StudentController extends AppController {
		public function index($a){
			$students = array('students' => $this->Student->find("first",array('conditions'=>array("student.id"=>$a))));
			$this->set(compact("students"));
		}

		public function delete ($id){
			$this->Student->delete($id);
       		$this->redirect(array('action'=>'edit'));
		}

		public function add ($param = "")
		{
			if ($param == "save")
			{
				$name = $_POST["name"];
				$group_id = $_POST["group_id"];
				$data = array("name" => $name, "group_id"=>$group_id);
				$this->Student->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
					$this->loadModel("Group");
					$groups = $this->Group->find("all");
					$this->set(compact("groups"));
		}
		public function edit ($id = "", $param = "view")
		{
			if ($param == "view"){


				$students;
				if ($id == "")
				{
					$students = $this->Student->find("all");
					$this->set(compact("students"));
					$this->render("viewList");
				}
				else{
					$student = $this->Student->find("first",array('conditions'=>array("Student.id"=>$id)));
					$this->loadModel("Group");
					$groups = $this->Group->find("all");
					$this->set(compact("student"));
					$this->set(compact("groups"));
					$this->render("viewForm");
				}

			}elseif ($param == "update")
			{
				$name = $_POST["name"];
				$group_id = $_POST["group_id"];
				$data = array('id' => $id, "name" => $name, "group_id"=>$group_id);
				$this->Student->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
		}
	}