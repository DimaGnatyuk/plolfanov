<?php
	class TeacherController extends AppController 
	{
		public function index(){
			$teachers = array('teacher' => $this->Teacher->find("first"));
			$this->set(compact("teachers"));
		}

		public function delete ($id){
			$this->Teacher->delete($id);
       		$this->redirect(array('action'=>'edit'));
		}

		public function add ($param = "")
		{
			if ($param == "save")
			{
				$name = $_POST["name"];
				$login = $_POST["login"];
				$password = $_POST["password"];
				$keyIdCertificate = $_POST["keyIdСertificate"];

				$data = array("name" => $name, "login"=>$login, "password"=>$password, "keyIdCertificate"=>$keyIdCertificate);
				$this->Teacher->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
					$teachers = $this->Teacher->find("all");
					$this->set(compact("teachers"));
		}
		public function edit ($id = "", $param = "view")
		{
			if ($param == "view"){


				$students;
				if ($id == "")
				{
					$teachers = $this->Teacher->find("all");
					$this->set(compact("teachers"));
					$this->render("viewList");
				}
				else{
					$teacher = $this->Teacher->find("first",array('conditions'=>array("Teacher.id"=>$id)));
					$this->set(compact("teacher"));
					$this->render("viewForm");
				}

			}elseif ($param == "update")
			{
				$name = $_POST["name"];
				$login = $_POST["login"];
				$password = $_POST["password"];
				$keyIdCertificate = $_POST["keyIdСertificate"];

				$data = array('id' => $id,"name" => $name, "login"=>$login, "password"=>$password, "keyIdCertificate"=>$keyIdCertificate);
				$this->Teacher->Save($data);
				$this->redirect(array('action'=>'edit'));
			}
		}
	}