<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
</head>
<body>
<ul>
	<li><a href="/group/add">Додати групу</a></li>
	<li><a href="/student/add">Додати студента</a></li>
	<li><a href="/teacher/add">Додати викладача</a></li>
	<li><a href="/subject/add">Додати предмет</a></li>
	<li><a href="/rating/add">Додати оцінки</a></li>
	<li><a href="/group/edit">Редагувати групу</a></li>
	<li><a href="/student/edit">Редагувати студента</a></li>
	<li><a href="/teacher/edit">Редагувати викладача</a></li>
	<li><a href="/subject/edit">Редагувати предмет</a></li>
	<li><a href="/rating/edit">Редагувати оцінки</a></li>
	<li><a href="/admin/index/exit">Вихід</a></li>
</ul>
</body>
</html>