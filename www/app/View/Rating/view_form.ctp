<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<h2>Edit Rating</h2>
	<form action="<?php echo $rating["Rating"]["id"];?>/update" method="POST">
<ul>
	<li>id: <?php echo $rating["Rating"]["id"];?></li>
	<li>rating: <input type="text" name="rating" value="<?php echo $rating["Rating"]["rating"];?>"></li>
    <li>student_id<select name="student_id">
	<?php
		foreach ($students as $student) : 
			$checked = "";
			if ($student["student"]["id"] == $rating["Rating"]["student_id"])
			{
				$checked = "selected";
			}
		?>
		<option <?php echo $checked;?> value="<?php echo $student["student"]["id"]?>"><?php echo $student["student"]["name"]?></option>
		<?php
			endforeach
		?>
	</select>
    </li>
    <li>subject_id<select name="subject_id">
	<?php
		foreach ($subjects as $subject) : 
			$checked = "";
			if ($subject["subject"]["id"] == $rating["Rating"]["subject_id"])
			{
				$checked = "selected";
			}
		?>
		<option <?php echo $checked;?> value="<?php echo $subject["subject"]["id"]?>"><?php echo $subject["subject"]["name"]?></option>
		<?php
			endforeach
		?>
	</select>
    </li>
	<li><input type="submit">
</ul>
<a href="/rating/delete/<?php echo $rating["Rating"]["id"];?>">Delete</a>
</form>
<a href="/admin">Back</a>
</body>
</html>