<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<h2>Edit Teacher</h2>
	<form action="<?php echo $subject["Subject"]["id"];?>/update" method="POST">
<ul>
	<li>id: <?php echo $subject["Subject"]["id"];?></li>
	<li>name: <input type="text" name="name" value="<?php echo $subject["Subject"]["name"];?>"></li>
    <li>teacher_id<select name="teacher_id">
	<?php
		foreach ($teachers as $teacher) : 
			$checked = "";
			if ($teacher["Teacher"]["id"] == $subject["Subject"]["teacher_id"])
			{
				$checked = "selected";
			}
		?>
		<option <?php echo $checked;?> value="<?php echo $teacher["Teacher"]["id"]?>"><?php echo $teacher["Teacher"]["name"]?></option>
		<?php
			endforeach
		?>
	</select>
    </li>
	<li><input type="submit">
</ul>
<a href="/subject/delete/<?php echo $subject["Subject"]["id"];?>">Delete</a>
<a href="/admin">Back</a>
</form>
</body>
</html>