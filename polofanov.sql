-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               5.5.25 - MySQL Community Server (GPL)
-- ОС сервера:                   Win32
-- HeidiSQL Версія:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for journal
CREATE DATABASE IF NOT EXISTS `journal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `journal`;


-- Dumping structure for таблиця journal.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table journal.groups: ~1 rows (приблизно)
DELETE FROM `groups`;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`) VALUES
	(1, 'ПС 4-1');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;


-- Dumping structure for таблиця journal.rating
CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idStudent` int(11) NOT NULL,
  `idSubject` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rating_student` (`idStudent`),
  KEY `FK_rating_groups` (`idSubject`),
  CONSTRAINT `FK_rating_groups` FOREIGN KEY (`idSubject`) REFERENCES `subject` (`id`),
  CONSTRAINT `FK_rating_student` FOREIGN KEY (`idStudent`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table journal.rating: ~1 rows (приблизно)
DELETE FROM `rating`;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` (`id`, `idStudent`, `idSubject`, `rating`, `data`) VALUES
	(1, 1, 1, 15, '2016-10-01');
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;


-- Dumping structure for таблиця journal.student
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `idGroup` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_student_groups` (`idGroup`),
  CONSTRAINT `FK_student_groups` FOREIGN KEY (`idGroup`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table journal.student: ~1 rows (приблизно)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `name`, `idGroup`) VALUES
	(1, 'Тестовий студент', 1);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;


-- Dumping structure for таблиця journal.subject
CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `idTeacher` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_subject_teacher` (`idTeacher`),
  CONSTRAINT `FK_subject_teacher` FOREIGN KEY (`idTeacher`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table journal.subject: ~1 rows (приблизно)
DELETE FROM `subject`;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` (`id`, `name`, `idTeacher`) VALUES
	(1, 'Тестовий предмет', 1);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;


-- Dumping structure for таблиця journal.teacher
CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `keyIdСertificate` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pass_login` (`pass`,`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table journal.teacher: ~1 rows (приблизно)
DELETE FROM `teacher`;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` (`id`, `name`, `login`, `pass`, `keyIdСertificate`) VALUES
	(1, 'Тест', 'цук', 'цукцук', '234234234');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
